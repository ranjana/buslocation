
package com.ranjana.BuslocationTracker;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/** Example resource class hosted at the URI path "/myresource"
 */
//This is working resoiurce file
@Path("/myresource")
public class MyResource {
    
    /** Method processing HTTP GET requests, producing "text/plain" MIME media
     * type.
     * @return String that will be send back as a response of type "text/plain".
     */
    @GET 
    @Produces("text/plain")
    public String getIt() {
        return "Hi there!";
    }

UserRepository myrepo = new UserRepository();
@GET
@Path("addstops")
@Produces("text/plain")
public String addstops() {
	myrepo.insertStop();
    return "Stop added";
}

@GET
@Path("test")
@Produces(MediaType.APPLICATION_XML)
public List<User> test() {
	System.out.println("inside test");	
	return UserRepository.test();
			
}
@GET
@Path("getAlluser")
@Produces(MediaType.APPLICATION_XML)
	public List<User> getusers() {
		System.out.println("start of user resourse........");
		return myrepo.getAllusers();
		
	}

@GET
@Path("getUserById/{id}")
@Produces(MediaType.APPLICATION_XML)
	public User getuser(@PathParam("id") int id) {
		System.out.println("start of getuser by id user resourse........");
		return myrepo.getUserById(id);
}
		

@POST
@Path("user")
public User createUser(User user) {
		System.out.println("start of mu user resourse  method getUserById ........");
		myrepo.createUser(user);
		return user;
		
	}

@GET
@Path("addstopTimes")
@Produces("text/plain")
public String addstopTimes() {
	myrepo.addstopTimes();
    return "Stop added";
}
}
